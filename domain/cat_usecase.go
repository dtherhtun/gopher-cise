package domain

import (
	"fmt"

	"gitlab.com/dtherhtun/gopher-cise/model"
)

type CatUseCase struct{}
type CatLang struct {
	*model.Cat
}

func (catusecase *CatUseCase) GetNewCat() *model.Cat {
	var something = &model.Cat{
		Name: "shweshwe",
	}
	return something
}

func (cat *CatLang) WithLang(lang string) {
	switch lang {
	case "mm":
		var something = &model.Cat{
			Name: cat.Name + " myanmar",
			Age:  cat.Age,
		}
		fmt.Println(*something)
	case "en":
		var something = &model.Cat{
			Name: cat.Name + " english",
			Age:  cat.Age,
		}
		fmt.Println(*something)
	}

}
